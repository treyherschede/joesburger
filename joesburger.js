//define the application
var app = angular.module('joesburger', []);

//define the controllers
app.controller('ctrlJoesBurger', function($scope) {
	$scope.getIngredients = function(list) {
		var str = "";
		for (i = 0; i < list.length - 1; i++) {
			str += list[i] + ", ";
		} //end for
		str += list[list.length - 1];
		return str;
	}; //end getIngredients()
	
	$scope.burgers = [];
	$scope.jsonData = "";
	$scope.jsonData += '[{"name":"Cheeseburger","ingredients":["Beef","American Cheese","Lettuce","Tomato","Special Sauce","Potato Bun"]},';
	$scope.jsonData += '{"name":"Cheddar Bacon Burger","ingredients":["Beef","Cheddar Cheese","Bacon","Grilled Onions","BBQ Sauce","Potato Bun"]},';
	$scope.jsonData += '{"name":"Mushroom Swiss Burger","ingredients":["Beef","Swiss Cheese","Sauteed Mushrooms","Fried Onion Strings","Mayonnaise","Potato Bun"]},';
	$scope.jsonData += '{"name":"A-1 Cheeseburger","ingredients":["Beef","American Cheese","Bacon","Grilled Onions","A-1 Sauce","Potato Bun"]},';
	$scope.jsonData += '{"name":"Double Diablo","ingredients":["Beef","Pepper Jack Cheese","Bacon","Jalapenos","Cholula","Mayonnaise","Potato Bun"]},';
	$scope.jsonData += '{"name":"Classic Crispy Chicken Sandwich","ingredients":["Breaded Chicken","Tomato","Lettuce","Pickles","Mayonnaise","Potato Bun"]}]';
	$scope.data = JSON.parse($scope.jsonData);
	for (i = 0; i < $scope.data.length; i++) {
		$scope.burgers.push($scope.data[i]);
	} //end for
	$scope.bannerStyle = {style:"w3-container w3-center w3-indigo"};
	$scope.navbarStyle = {style:"w3-bar w3-black"};
	$scope.navbarButtonStyle = {style:"w3-bar-item w3-button w3-mobile"};
	$scope.homeStyle = {style:"",
	  header:"w3-center",
	  imgRow:"w3-row w3-margin-top w3-margin-bottom",
	  imgCol:"w3-third w3-padding",
	  imgStyle:"w3-card",
	  imgCaption:"w3-white w3-xlarge w3-padding",
	  table:"w3-table w3-centered"};
	$scope.menuStyle = {style:""};
	$scope.contactStyle = {style:"w3-container w3-padding",
	  p:"w3-large",
	  input:"w3-input w3-padding-16",
	  btn:"w3-bar w3-indigo w3-button w3-section"};
	$scope.aboutStyle = {style:"w3-container",
	  jumbotron:"",
	  body:"",
	  list:"w3-ul"};
	$scope.rowStyle = {style:"w3-row-padding"};
	$scope.colStyle = {style:"w3-third w3-margins w3-padding-16"};
	$scope.menuItemStyle = {style:"w3-card w3-border-black w3-border",
	  textStyle:"w3-white",
      nameStyle:"w3-center w3-indigo",
	  descStyle:"w3-padding"};
	
	$scope.iHome = "fas fa-home fa-lg w3-padding";
	$scope.iMenu = "fas fa-utensils fa-lg w3-padding";
	$scope.iContact = "fas fa-question fa-lg w3-padding";
	$scope.iAbout = "fas fa-info fa-lg w3-padding";
	
	$scope.menuItem1 = {name:$scope.burgers[0].name,
	  desc:$scope.getIngredients($scope.burgers[0].ingredients)};
	$scope.menuItem2 = {name:$scope.burgers[1].name,
	  desc:$scope.getIngredients($scope.burgers[1].ingredients)};
	$scope.menuItem3 = {name:$scope.burgers[2].name,
	  desc:$scope.getIngredients($scope.burgers[2].ingredients)};
	$scope.menuItem4 = {name:$scope.burgers[3].name,
	  desc:$scope.getIngredients($scope.burgers[3].ingredients)};
	$scope.menuItem5 = {name:$scope.burgers[4].name,
	  desc:$scope.getIngredients($scope.burgers[4].ingredients)};
	$scope.menuItem6 = {name:$scope.burgers[5].name,
	  desc:$scope.getIngredients($scope.burgers[5].ingredients)};
	
	$scope.goHome = function() {
		document.getElementById("home").style.display = "block";
		document.getElementById("menu").style.display = "none";
		document.getElementById("contact").style.display = "none";
		document.getElementById("about").style.display = "none";
	}; //end goHome()
	
	$scope.goMenu = function() {
		document.getElementById("home").style.display = "none";
		document.getElementById("menu").style.display = "block";
		document.getElementById("contact").style.display = "none";
		document.getElementById("about").style.display = "none";
	}; //end goMenu()
	
	$scope.goContact = function() {
		document.getElementById("home").style.display = "none";
		document.getElementById("menu").style.display = "none";
		document.getElementById("contact").style.display = "block";
		document.getElementById("about").style.display = "none";
	}; //end goContact()
	
	$scope.goAbout = function() {
		document.getElementById("home").style.display = "none";
		document.getElementById("menu").style.display = "none";
		document.getElementById("contact").style.display = "none";
		document.getElementById("about").style.display = "block";
	}; //end goAbout()
}); //end controller